#!/bin/sh
./startlin.py
echo Starting send loop \(ctrl+c to exit\) ...
while true; do
  sleep 5
  echo Sending unanswered master frame ...
  cansend sllin0 001#
  sleep 5
  echo Sending master frame and slave response ...
  cansend sllin0 002#1234
done

