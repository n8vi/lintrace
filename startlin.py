#!/usr/bin/python3

from os import path,uname,chdir,getcwd
from contextlib import contextmanager
import subprocess
import sys

def callign(args, p="", cwd='.'):
    popen = subprocess.Popen(args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for stdout_line in popen.stdout:
        print(p+"> "+stdout_line.decode("utf-8"), end="")
    popen.stdout.close()

def call(args, p="", cwd='.'):
    popen = subprocess.Popen(args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    for stdout_line in popen.stdout:
        print(p+"> "+stdout_line.decode("utf-8"), end="")
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        print(p+ "error executing %s.  Aborting."%" ".join(args))
        sys.exit(1)

def install_headers(p=""):
    if path.exists('/lib/modules/%s/build'%uname().release):
        print(p+ "Headers already installed.  Doing nothing.")
        return
    call(['sudo','apt-get','update'],p)
    call(['sudo','apt-get','install','linux-headers-%s'%uname().release],p)

def fetch_module(p=""):
    if path.exists('linux-lin'):
        print(p+"Module already fetched.  Doing nothing.")
        return
    call(["/usr/bin/git", "clone", "git://rtime.felk.cvut.cz/linux-lin.git"],p)

def build_module(p=""):
    if path.exists('linux-lin/sllin/sllin.ko'):
        print(p+"Module already built.  Doing nothing.")
        return
    print(p+"Fetching kernel module")
    fetch_module(p+"  ")
    print(p+"Done fetching kernel module.  Installing kernel headers ... ")
    install_headers(p+"  ")
    print(p+"Done installing kernel headers.")
    call(['pwd'],p,'linux-lin/sllin')
    call(['/bin/sh','-c','/bin/echo $PWD'],p,'linux-lin/sllin')
    call(['/bin/sh', '-c', 'make'],p,'linux-lin/sllin')

def load_module(p=""):
    if path.exists('/sys/module/sllin'):
        print(p+ "Module already loaded.  Doing nothing.")
        return
    print(p+"Building kernel module ...")
    build_module(p+"  ")
    print(p+"Done building kernel module.")
    call(['sudo','insmod','linux-lin/sllin/sllin.ko'],p)

def load_pru232d(p=""):
    print(p+"Detaching line discipline ...")
    callign(["pkill", "-9", "ldattach"], p) 
    print(p+"Killing PRU code manager ...")
    callign(["pkill", "-9", "pru232d"],p) 
    print(p+"Building PRU code manager ...")
    call(["make"], p)
    print(p+"Starting PRU code manager ...")
    call(['config-pin', 'P8.45', 'pruout'],p)
    call(['config-pin', 'P9.24', 'uart'],p)
    call(['config-pin', 'P9.26', 'uart'],p)
    call(['config-pin', 'P9.31', 'pruin'],p)
    # call(["../pru-232/rebaudd"])
    print(p+"PRU code manager started.")
    

def up_interface(p="", ldesc=26):
    # if path.exists('/sys/class/net/sllin0'):
    #     print(p+ "Interface already up.  Doing nothing.")
    #     return
    print(p+"Loading kernel module ...")
    load_module(p+"  ")
    print(p+"Done loading kernel module.")
    # call(['config-pin', 'P9.31', 'pru'],p)
    # subprocess.call(['sudo','pkill','ldattach'])
    load_pru232d(p+"  ")
    print(p+"Attaching line discipline ...")
    call(['sudo','ldattach',str(ldesc),'/dev/ttyS1'],p)
    print(p+"Line discipline attached.")
    call(['sudo','ip','link','set','sllin0','up'],p)


def main():
    if len(sys.argv) == 2:
        ldesc = int(argv[1])
    else:
        ldesc = 25
    print("Bringing up interface ...")
    up_interface("  ", ldesc)
    print("Done bringing up interface.")

if __name__== "__main__":
   main()
