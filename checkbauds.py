#!/usr/bin/python3

import subprocess
import os

FNULL = open(os.devnull, 'w')

for b in range(5000000):
    r = subprocess.call(["stty", "-F", "/dev/ttyS1", str(b)], stdout=FNULL, stderr=subprocess.STDOUT)
    if r == 0:
        print("Valid baud rate: ", b)
    
