#include <stdio.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include <stdlib.h>

#include "util.h"

int pruinit ()
{
  int ret;
  tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;

  prussdrv_init();

  ret = prussdrv_open(PRU_EVTOUT_0);
  if (ret) {
    printf("prussdrv_open(PRU_EVTOUT_0) failed ( == %d) \n", ret);
    exit (ret);
    }

  ret = prussdrv_open(PRU_EVTOUT_1);
  if (ret) {
    printf("prussdrv_open(PRU_EVTOUT_1) open failed ( == %d) \n", ret);
    exit (ret);
    }

  prussdrv_pruintc_init(&pruss_intc_initdata);

  return(0);
}

volatile unsigned char *prudram(int prunum)
{
  volatile unsigned char *pruDataMem = (unsigned char *)0;

  //Initialize pointer to PRU data memory
  if (prunum == 0) {
    prussdrv_map_prumem (PRUSS0_PRU0_DATARAM, (void**)&pruDataMem);
  } else if (prunum == 1) {
    prussdrv_map_prumem (PRUSS0_PRU1_DATARAM, (void**)&pruDataMem);
    }

  return pruDataMem;
}
