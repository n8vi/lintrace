
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#define __USE_XOPEN_EXTENDED
#include <stdlib.h>
// #include <termios.h>
#include <fcntl.h>
#include "util.h"
#include "rx_bin.h"

#define PRUNUM 0
#define IRAM (PRUNUM+2)

void linattach(char *pty)
{
    static int wstat;
    pid_t pid;


    pid = vfork();
    if (pid < 0)  {
        perror("fork");
        exit(1);
        }
    if (pid == 0) {
        daemon(0,0);
        execl("/usr/sbin/ldattach", "ldattach", "25", pty, NULL);
        perror("execl()");
        exit(1);
    } else {
        wait(&wstat);
        }
}

int main (int argc, char **argv)
{
    unsigned short* wpos;
    unsigned short* rpos;
    char *dram;
    int f0, f1;
    fd_set rfds;
    struct timeval tv;
    int retval;
    int i;
    int masterfd;
    unsigned char meta;
    char byte;
    struct termios ser;

    pruinit();

    prussdrv_pru_disable ( 0 );
    prussdrv_pru_disable ( 1 );
    prussdrv_pru_write_memory(IRAM, 0, (unsigned int *)rx, sizeof(rx));
    prussdrv_pru_enable ( PRUNUM );

    f0 = prussdrv_pru_event_fd(PRU_EVTOUT_0);
    f1 = prussdrv_pru_event_fd(PRU_EVTOUT_1);

    prussdrv_pru_clear_event (PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);
    prussdrv_pru_clear_event (PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);

    prussdrv_map_prumem (PRUSS0_PRU0_DATARAM, (void**)&wpos);

    rpos = wpos + 1;

    dram = (char *)wpos + 4096;

    *rpos = 0;

    // masterfd = open("/dev/ptmx", O_RDWR);
    // grantpt(masterfd);
    // unlockpt(masterfd);
    // printf("Connecting PRU UART to %s\n", ptsname(masterfd));


    masterfd = open("/dev/ttyS2", O_RDWR);

    int res = tcgetattr(masterfd, &ser);
    if (res < 0) {
        perror("tcgetattr()");
        return 2; 
        }
    cfsetspeed(&ser,B19200);

    if ((res = tcsetattr(masterfd, TCSANOW, &ser)) < 0){
        perror("tcsetattr()");
        return 3;
        }

    // printf("Attaching line discipline ...\n");
    // linattach(ptsname(masterfd));
    printf("Becoming daemon ...\n");
    daemon(0,0);

    while (1) {

        FD_ZERO(&rfds);
        FD_SET(f0, &rfds);
        FD_SET(f1, &rfds);

        tv.tv_sec = 5;
        tv.tv_usec = 0;

        retval = select(f1+1, &rfds, NULL, NULL, &tv);

        if (retval == -1) {
            perror("select()");
        } else {
            if (retval && FD_ISSET(f0, &rfds)) {
                read(f0, &i, 4);
                prussdrv_pru_clear_event (PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);
                while (*wpos != *rpos) {
                    (*rpos)++;
                    meta = (unsigned char)dram[(*rpos)-1];
                    (*rpos)++;
                    byte = dram[(*rpos)-1];
                    (*rpos) %= 4096;
                    if (meta == 0 && byte == 0) {
                        printf("{BRK}");
                        tcsendbreak(masterfd, 1);
                    } else if (meta == 0) {
                        printf("{FRM}");
                    } else {
                    write(masterfd, &(dram[(*rpos)-1]), 1);
                    // write(masterfd, &(dram[(*rpos)-1]), 1);
                    // write(1, &(dram[(*rpos)-1]), 1);
                        printf("%c", byte);
                        }
                    }
                fflush(stdout);
                }
            }
        }
        return(0);
}

