CC=gcc -ggdb -Wall -D__DEBUG -mtune=cortex-a8 -march=armv7-a
LDLIBS=-lprussdrv

all: pru232d

pru232d: pru232d.o util.o
	$(CC) -o $@ $< util.o $(LDLIBS)

pru232d.o: pru232d.c rx_bin.h
	${CC} -c -o $@ $<

%_bin.h: %.p
	pasm -V2 -C$(basename $(notdir $<)) -c $<

clean:
	rm -fr *.bin *~ *_bin.h *.o .*.swp pru232d *.log
