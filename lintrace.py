#!/usr/bin/python3

from startlin import up_interface
from subprocess import call
import curses
from datetime import datetime
from time import sleep,time

try:
    import can
except ImportError:
    call(["apt-get", "update"])
    call(["apt-get", "install", "python3-can"])
    import can

def winmain(stdscr):
    stdscr.clear()

    win = curses.newwin(1, curses.COLS, curses.LINES-1, 0)
    wlog = curses.newwin(curses.LINES, curses.COLS, 0, 0)
    wlog.scrollok(True)
    stdscr.nodelay(True)
    s = ''

    while True:
        date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            c = stdscr.getkey()
        except:
            win.clear()
            win.addstr(date_time+"> "+s, curses.A_BOLD)
            win.refresh()
            msg = bus.recv(0.2)

# >>> msg = bus.recv()
# >>> msg
# can.Message(timestamp=1587129939.247782, is_remote_frame=False, extended_id=True, is_error_frame=False, arbitration_id=0x8001, dlc=0, data=[])
# >>> msg = bus.recv()
# >>> msg

# 2020-04-17 13:57:11> WTF: ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__le__', '__len__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'arbitration_id', 'data', 'dlc', 'id_type', 'is_error_frame', 'is_extended_id', 'is_remote_frame', 'timestamp']

            if msg is not None:
                fullframe = False
                if msg.is_extended_id:
                    idtype = "  -> EFF"
                    dispo = "(out-of-band metadata / error)"
                else:
                    idtype = "SFF"
                if msg.is_remote_frame:
                    frametype = " RTR"
                    if idtype == 'SFF':
                        dispo = "(request from master)"
                else:
                    frametype = "NRTR"
                    if idtype == 'SFF':
                        dispo = "(response from slave / complete frame)"
                        fullframe = True
                datastr = ''.join('%0.2x'%x for x in msg.data)
                linadr = msg.arbitration_id & 0x3f
                flags = ""
                if msg.arbitration_id & (1 << 6):
                    flags += " CCHRSP"
                if msg.arbitration_id & (1 << 7):
                    flags += " CHKEXT"
                if msg.arbitration_id & (1 << 8):
                    flags += " SYNRSP"
                if msg.arbitration_id & (1 << 14):
                    flags += " RXTIMO"
                if msg.arbitration_id & (1 << 15):
                    flags += " ERRCKS"
                if msg.arbitration_id & (1 << 16):
                    flags += " FRMERR"
                if fullframe:
                    smsg = "%0.3x: %s %s %d %s %s %s"%(linadr, idtype, frametype, msg.dlc, datastr, flags, dispo)
                    wlog.addstr(date_time+"> "+smsg+"\n")
                    fp.write(date_time+"> "+smsg+"\n")
                    wlog.refresh()
            continue
        s += c
        if c in ("\r","\n","\r\n"):
            wlog.addstr(date_time+"> "+s)
            fp.write(date_time+"> "+str(s))
            wlog.refresh()
            s = ''
            win.clear()
            win.addstr(date_time+"> "+s, curses.A_BOLD)
            win.refresh()
        elif c == "KEY_BACKSPACE":
            s = s[:-14]
            win.clear()
            win.addstr(date_time+"> "+s, curses.A_BOLD)
            win.refresh()
        elif c in ("\x1b", "\x04"):
            # close file
            return
        else:
            win.clear()
            win.addstr(date_time+"> "+s, curses.A_BOLD)
            win.refresh()

def main():
    global bus 
    global fp

    fn = "%s.log"%int(time())
    fp = open(fn, "w")

    print("Bringing up interface ...")
    up_interface("  ")
    print("Done bringing up interface.")

    bus = can.interface.Bus(bustype='socketcan', channel='sllin0')

    curses.wrapper(winmain)

    fp.close()

    print("Shutdown complete.  Log written to %s."%fn)

if __name__== "__main__":
   main()
